
### Libraries
import sys as SYS
import os as OS
from tqdm import tqdm
import numpy as NP
import pandas as PD
import scipy.stats as STATS
import scipy.interpolate as INTP
import matplotlib as MPL
import matplotlib.pyplot as PLT

import skimage as SKI
import skimage.io as SKIO
import skimage.exposure as SKIE
import skimage.color as SKIC
import skimage.measure as SKIM


### user parameters
rec = '20210706_Rotated_PIC_0036_c_0h' # a unique label for this particular recording
linelenght_m_per_pixel = 0.04/368.2 # m/px
n_sample_images = 10

fps_in = 50
fps_out = 10 # only for image export


## TODO:
# rotate images/contours a bit to make them perfectly horizontal
# glob not working on windows?


### Inspection Plot
# https://scikit-image.org/docs/dev/auto_examples/color_exposure/plot_equalize.html#sphx-glr-auto-examples-color-exposure-plot-equalize-py
def PlotImgAndHist(image, axes, bins=256):
    """Plot an image along with its histogram and cumulative histogram.

    """
    image = SKI.img_as_float(image)
    ax_img, ax_hist = axes
    ax_cdf = ax_hist.twinx()

    # Display image
    ax_img.imshow(image, cmap=PLT.cm.gray, origin = 'upper')
    ax_img.set_axis_off()

    # Display histogram
    ax_hist.hist(image.ravel(), bins=bins, histtype='step', color='black')
    ax_hist.ticklabel_format(axis='y', style='scientific', scilimits=(0, 0))
    ax_hist.set_xlabel('Pixel intensity')
    ax_hist.set_xlim(0, 1)
    ax_hist.set_yticks([])

    # Display cumulative distribution
    img_cdf, bins = SKIE.cumulative_distribution(image, bins)
    ax_cdf.plot(bins, img_cdf, 'r')
    ax_cdf.set_yticks([])

    return ax_img, ax_hist, ax_cdf


### Contour Extraction
def SingleFrameContours(image_path, plot = True, show = True, fixed_cutoffs = None):


    # load the image
    img = SKIO.imread(image_path)
    image_width = img.shape[1]

    # Contrast stretching
    p2, p98 = NP.percentile(img, (5, 95))
    exp = SKIE.rescale_intensity(img, in_range=(p2, p98))

    # exp = SKIE.equalize_hist(img)

    # # Adaptive Equalization
    # exp = SKIE.equalize_adapthist(exp, clip_limit=0.01)

    # convert to grayscale
    gray = SKIC.rgb2gray(exp)

    ## contour contrast cutoff confirmation
    # this function either receives user-specified cutoffs
    # or scans the histogram for good ones.
    #    Good is defined as thode that give relatively straight lines, i.e. sharp contours

    # Find contours at a constant value of c
    if fixed_cutoffs is not None:
        # (a) user input cutoffs
        cutoffs = [cut for _, cut in fixed_cutoffs.items()]
    else:
        # (b) step scan
        # cutoffs = NP.linspace(0.1, 0.9, 17, endpoint = True)
        cutoffs = NP.linspace(0.05, 0.95, 91, endpoint = True)


    # perform contour extraction with different cutoffs
    identified_contours = {}

    for cutoff in cutoffs:
        identified_contours[cutoff] = []
        contours = SKIM.find_contours(gray, cutoff)

        for contour in contours:

            ### good contours must go all the way from left to right!
            # (1) ninimum: number of points of a straight line
            sufficient_length = contour.shape[0]
            if not (sufficient_length >= image_width): 
                continue

            # (2) covered x pixel position range
            covered_xdistance = contour[:, 1].max() - contour[:, 1].min()
            if covered_xdistance <= (0.95 * image_width):
                continue

            # store smoothness
            smoothness = NP.mean(NP.abs(NP.diff(contour[:, 0])))
            # print (smoothness)

            # also, position consistency is a criterium
            mean_y = NP.mean(contour[:, 0])

            # => append only good contours 
            identified_contours[cutoff].append([smoothness, mean_y, contour])

        # # warn if too many contours found
        # if len(identified_contours[cutoff]) > 2:
        #     print (f"{image_path}, {len(identified_contours[cutoff])} contours found for cutoff = {cutoff}")


    ## find midline
    # all the contours should lie somewhere around a common midline.
    meanlines = []
    for cutoff, contours in identified_contours.items():
        if len(contours) > 1:
            meanlines.append([cutoff, NP.mean([cont[1] for cont in contours])])

    meanlines = NP.stack(meanlines, axis = 0)
    # print (meanlines)

    # regresson (from mean points)
    line = STATS.linregress(meanlines[:, 0], meanlines[:, 1])
    # print (line)

    # function to access the midline
    GetMidline = lambda cutoff: line.intercept + line.slope * cutoff


    ### group contours above and below midline
    grouped_aboveornot = {tf: {'smoothness': [], 'contours': [], 'cutoffs': []} for tf in [True, False]}
    for cutoff in cutoffs:
        for smoothness, mean_y, contour in identified_contours[cutoff]:
            # print (smoothness, mean_y, contour)
            aboveornot = mean_y > GetMidline(cutoff)

            # append data
            grouped_aboveornot[aboveornot]['cutoffs'].append(cutoff) 
            grouped_aboveornot[aboveornot]['smoothness'].append(smoothness) 
            grouped_aboveornot[aboveornot]['contours'].append(contour) 


    ## select best (smoothest) contours
    # also to find best cutoffs in test scan
    smoothest_contours = {}
    auto_cutoffs = {}
    for aboveornot, contourdict in grouped_aboveornot.items():
        # print (aboveornot, contourdict)

        smoothest = NP.argmin(contourdict['smoothness'])
        smoothest_contours[aboveornot] = contourdict['contours'][smoothest]
        auto_cutoffs[aboveornot] = contourdict['cutoffs'][smoothest]


    # optional: plot the detection outcome
    if plot:
        # fig, axes = PLT.subplots(nrows=2, ncols=1, sharex=False, sharey=False, gridspec_kw = {'height_ratios': [4,1]})
        dpi = 300
        fig, ax = PLT.subplots(**{'figsize': (1920/dpi, 1080/dpi), 'dpi': dpi })
        fig.subplots_adjust(left = 0., top = 1., right = 1., bottom = 0.)

        ax.spines[:].set_visible(False)

        ax.imshow(img, origin = 'upper', zorder = 0)
        # PlotImgAndHist(gray, axes)
        # axes[1].axvline(cutoff, color = 'r', ls = '--', lw = 1)


    ## Interpolation
    # we only need a certain number of sample points
    x_scan = NP.linspace(0., image_width, 129, endpoint = True)[1:-1]
    x_intp = NP.linspace(0., image_width, (image_width//5)+1, endpoint = True)[1:-1]

    interpolated_contours = {}
    for aboveornot, contour in smoothest_contours.items():
        contour_sorted = contour[contour[:,1].argsort(), :]

        samples = [NP.argmax(contour_sorted[:, 1] > x) for x in x_intp]

        x = contour_sorted[samples, 1]
        y = contour_sorted[samples, 0]

        # Interpolation
        ContourInterpolation = INTP.Rbf(x, y, function = 'thin_plate', smooth = 5)
        y_intp = ContourInterpolation(x_intp)

        # axes[0].plot(contour[:, 1], contour[:, 0], linewidth=2, color = (cutoff, 0., 0.))
        if plot:
            ax.plot(x_intp, y_intp, linewidth=2, color = (0., cutoff, 0., 0.6), zorder = 10)


        samples = [NP.argmax(contour_sorted[:, 1] > x) for x in x_scan]

        x_points = x_scan# contour_sorted[samples, 1]
        y_points = ContourInterpolation(x_points)#contour_sorted[samples, 0]

        # store the interpolated contour
        interpolated_contours[str(aboveornot).lower()] = y_points
        
        if plot:
            ax.scatter(x_points, y_points, s = 5, edgecolor = 'y', facecolor = 'none', zorder = 20)

    if plot:
        if show:
            # either show the image (for test scan)
            PLT.show()
        else:
            # or store the frame for visual confirmation (rollout on video)
            fig.savefig(image_path.replace('frames', 'frames_out'), transparent = False, dpi = dpi)
            PLT.close('all')


    # clean up the interpolated contours and return them
    interpolated_contours = PD.DataFrame.from_dict(interpolated_contours).loc[:, ['false', 'true']]
    interpolated_contours.columns = ['lower', 'upper']
    interpolated_contours.index = x_scan
    interpolated_contours['distance'] = NP.abs(interpolated_contours['upper'] - interpolated_contours['lower'])
    # print (interpolated_contours)

    return auto_cutoffs, interpolated_contours





### test single image
def TestSingleImage(image_path, fixed_cutoffs):
    return SingleFrameContours(image_path \
                               , show = True \
                               , fixed_cutoffs = fixed_cutoffs \
                               )



### find best cutoffs
def FindBestCutoff(basedir = 'frames'):
    # extract contours naively for a subset of images

    frames = OS.listdir(basedir)
    cuts = {True: [], False: []}
    for image_path in tqdm(list(sorted(frames))[::len(frames)//n_sample_images]):
        best_cut, interpolated_contours = SingleFrameContours(f"{basedir}{OS.sep}{image_path}", plot = False)

        for tf, cut in best_cut.items():
            cuts[tf].append(cut)

    scan_contrasts = PD.DataFrame.from_dict(cuts)
    
    auto_cutoffs = scan_contrasts.median()
    print('best cutoffs:', auto_cutoffs)

    scan_contrasts.to_csv(f'results{OS.sep}scan_contrasts_{rec}.csv', sep = ";")
    
    
    return {tf: auto_cutoffs[tf] for tf in [False, True]}


### loop folder
def PerformContourExtraction(basedir = 'frames', *args, **kwargs):
    # data extraction
    # cutoffs: gray value thresholds to find contour from above (True) or below (False)

    if kwargs['plot'] and not kwargs['show']:
        PLT.ioff()

    frames = OS.listdir(basedir)

    pointdiffs = []
    frames_idx = []
    framelist = list(sorted(frames))
    for idx in tqdm(range(len(framelist))):
        image_path = framelist[idx]
        try:
            kws = kwargs.copy()
            if not idx % (fps_in//fps_out) == 0:
                kws['plot'] = False
                kws['show'] = False
                

            # print (idx, kws)
            best_cut, interpolated_contours = SingleFrameContours(f"{basedir}{OS.sep}{image_path}", *args, **kws)

            pointdiffs.append(interpolated_contours.loc[:, ['distance']].T)
            frames_idx.append(int(OS.path.splitext(OS.path.split(image_path)[-1])[0]))
        except Exception as e:
            print (e)
            continue

    # print (pointdiffs)
    pointdiffs = PD.concat(pointdiffs, axis = 0)
    # pointdiffs.to_csv('results{OS.sep}pointdiffs.csv', sep = ';')
    # print (pointdiffs)
    pointdiffs.index = frames_idx
    pointdiffs.index.name = 'frame_nr'

    return pointdiffs




def LoadPointdiffFile(pdfilepath):
    # loads a previously stored data file

    data = PD.read_csv(f'results{OS.sep}pointdiffs_{rec}.csv', sep = ';')
    data.set_index('frame_nr', inplace = True)

    return data



def ConvertPointdiffsSI(data):
    # spatial and temporsl unit conversion

    data.columns = [float(col)*linelenght_m_per_pixel for col in data.columns]
    data.columns.name = 'position_m'

    # also convert distances
    data.loc[:, :] *= linelenght_m_per_pixel

    # time
    time = NP.round(data.index.values / fps_in, 2)
    data.index = time
    data.index.name = 'time_s'


    return data


if __name__ == "__main__":

    ## I. Test run on a single image
    # image_path = "frames/00101.png"
    # TestSingleImage(image_path, fixed_cutoffs = {True: 0.64, False: 0.68} )
    # pause

    ## II. Find best threshold
    basedir = 'frames'
    # auto_cutoffs = FindBestCutoff(basedir)

    ## III. Full Data Extraction
    pointdiffs = PerformContourExtraction(  basedir \
                                          # , fixed_cutoffs = auto_cutoffs \
                                          , fixed_cutoffs = {True: 0.64, False: 0.68} \
                                          , plot = True, show = False \
                                          )

    # filepath for saving
    pdfilepath = f'results{OS.sep}pointdiffs_{rec}.csv'

    # if file was saved, it can be re-loaded with this function:
    # pointdiffs = LoadPointdiffFile(pdfilepath)

    # Convert to SO units
    pointdiffs = ConvertPointdiffsSI(pointdiffs)

    pointdiffs.to_csv(f'results{OS.sep}pointdiffs_{rec}.csv', sep = ';')
