import sys as SYS
import os as OS
import numpy as NP
import pandas as PD

import skimage.io as SKIO


rec = '20210706_Rotated_PIC_0036_c_0h'



def LoadData():
    data = PD.read_csv(f'results{OS.sep}pointdiffs_{rec}.csv', sep = ';')
    data.set_index('time_s', inplace = True)
    time = NP.round(data.index.values, 2)
    data.index = time
    data.index.name = 'time (s)'


    data.columns = NP.round(NP.array(data.columns.values, dtype = float), 4)
    data.columns.name = 'position (m)'

    # data = data.T
    mean = NP.mean(data.values, axis = (0,1))
    data.loc[:, :] -= mean
    std = 1*NP.std(data.values, axis = 0)
    data.loc[:, :] /= std
    # data = data.T

    timeselection = (time >= 0) & (time <= 1000)
    data = data.loc[timeselection, :]
    time = time[timeselection]


    print (data)


    # normalize rows
    for col in data.columns:
        data.loc[:, col] -= NP.nanmean(data[col].values)
        data.loc[:, col] /= NP.nanstd(data[col].values)

    return data



def ScaleNormalizeImage(data):

    vals = -1*data.values
    vals -= NP.min(vals)
    vals /= NP.max(vals)

    vals *= 255.
    return NP.array(vals, dtype = NP.uint8)


if __name__ == "__main__":
    # load the data
    data = LoadData()

    # turn data into a scaled image
    img = ScaleNormalizeImage(data)

    # save the image
    SKIO.imsave(f'results{OS.sep}dataimage_{rec}.png', img)
