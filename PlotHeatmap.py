import sys as SYS
import os as OS
from tqdm import tqdm
import numpy as NP
import pandas as PD
import scipy.stats as STATS
import scipy.interpolate as INTP
import matplotlib as MPL
import matplotlib.pyplot as PLT
import seaborn as SB

import skimage as SKI
import skimage.io as SKIO
import skimage.exposure as SKIE
import skimage.color as SKIC
import skimage.measure as SKIM


rec = '20210706_Rotated_PIC_0036_c_0h'


if __name__ == "__main__":

    data = PD.read_csv(f'results{OS.sep}pointdiffs_{rec}.csv', sep = ';')
    data.set_index('time_s', inplace = True)
    time = NP.round(data.index.values, 2)
    data.index = time
    data.index.name = 'time (s)'


    data.columns = NP.round(NP.array(data.columns.values, dtype = float), 4)
    data.columns.name = 'position (m)'

    # data = data.T
    mean = NP.mean(data.values, axis = (0,1))
    data.loc[:, :] -= mean
    std = 1*NP.std(data.values, axis = 0)
    data.loc[:, :] /= std
    # data = data.T

    timeselection = (time >= 0) & (time <= 1000)
    data = data.loc[timeselection, :]
    time = time[timeselection]


    print (data)


    # normalize rows
    for col in data.columns:
        data.loc[:, col] -= NP.nanmean(data[col].values)
        data.loc[:, col] /= NP.nanstd(data[col].values)


    SB.set_theme()


    # Draw a heatmap with the numeric values in each cell
    dpi = 300
    fig, (ax, cbar) = PLT.subplots(1, 2, figsize=(4096/dpi, 1024/dpi), gridspec_kw = {"width_ratios": (.98, .02), "wspace": .05})
    fig.subplots_adjust(left = 0.06, bottom = 0.28, top = 0.92, right = 0.94)
    # fig.subplots_adjust(left = 0.1, bottom = 0.1, top = 0.94, right = 0.92)
    SB.heatmap(data.T \
                , annot=False \
                , linewidths=0. \
                , ax=ax \
                , cbar_ax=cbar \
                , cbar_kws={"orientation": "vertical"} \
                , cmap = 'RdBu' \
                )

    xticks = NP.linspace(0, data.shape[0], 11, endpoint = True)
    ax.set_xticks(xticks)
    ax.set_xticklabels( NP.round(NP.linspace(NP.min(time), NP.max(time), len(ax.get_xticks()), endpoint = True)) )
    # yticks = np.arange(0, thick_df.T.shape[1], 1) + .5

    cbar.set_ylabel('row-wise standard deviations of point distance')

    fig.suptitle(rec)
    # PLT.tight_layout()
    fig.savefig(f'results{OS.sep}heatmap_{rec}.png', dpi = dpi, transparent = False)
    PLT.show()
